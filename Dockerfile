# This is a Dockerfile to be used with OpenShift3

FROM rhel7

MAINTAINER Christoph Görn <goern@redhat.com>
# based on the work of Takayoshi Kimura <tkimura@redhat.com>

ENV container docker
ENV MATTERMOST_VERSION 3.0.3

# Labels consumed by Red Hat build service
LABEL Component="mattermost" \
      Name="projectatomic/mattermost-210-rhel7" \
      Version="3.0.3" \
      Release="1"

# Labels could be consumed by OpenShift
LABEL io.k8s.description="Mattermost is an open source, self-hosted Slack-alternative" \
      io.k8s.display-name="Mattermost 3.0.3" \
      io.openshift.expose-services="8065:mattermost" \
      io.openshift.tags="mattermost,slack"

# Labels could be consumed by Nulecule Specification
#LABEL io.projectatomic.nulecule.environment.required="MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE" \
#      io.projectatomic.nulecule.environment.optional="VOLUME_CAPACITY" \
#      io.projectatomic.nulecule.volume.data="/var/lib/psql/data,1Gi"

RUN yum update -y --setopt=tsflags=nodocs && \
    yum install -y --setopt=tsflags=nodocs tar nc && \
    yum clean all

#RUN cd /opt && \
#    curl -LO https://releases.mattermost.com/2.1.0/mattermost-team-2.1.0-linux-amd64.tar.gz && \
#    tar xf mattermost-team-2.1.0-linux-amd64.tar.gz &&\
#    rm mattermost-team-2.1.0-linux-amd64.tar.gz

RUN cd /opt && \
    curl -LO https://releases.mattermost.com/3.0.3/mattermost-team-3.0.3-linux-amd64.tar.gz && \
    tar xf mattermost-team-3.0.3-linux-amd64.tar.gz &&\
    rm mattermost-team-3.0.3-linux-amd64.tar.gz

COPY mattermost-launch.sh /opt/mattermost/bin/mattermost-launch.sh
COPY config.json /opt/mattermost/config/config.json
RUN chmod +x /opt/mattermost/bin/mattermost-launch.sh
RUN chmod 777 /opt/mattermost/config/config.json && \
    mkdir /opt/mattermost/data && \
    chmod 777 /opt/mattermost/logs/ /opt/mattermost/data

# Make sure we can run with random UID, even in standalone docker
EXPOSE 8065 

WORKDIR /usr/bin/ 
CMD nc -z 172.30.220.10 3306

WORKDIR /opt/mattermost/bin
CMD /opt/mattermost/bin/mattermost-launch.sh

#WORKDIR /opt/mattermost/bin
#CMD /opt/mattermost/bin/platform
