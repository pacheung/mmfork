#!/bin/bash -x
config=/opt/mattermost/config/config.json
DB_HOST=${DB_HOST:-mmopenpaas.cnym3nmwgp5j.ap-southeast-1.rds.amazonaws.com}
DB_PORT_5432_TCP_PORT=${DB_PORT_5432_TCP_PORT:-3306}
MM_USERNAME=${MM_USERNAME:-mmuser}
MM_PASSWORD=${MM_PASSWORD:-mmuser_password}
MM_DBNAME=${MM_DBNAME:-mattermost}

# Backup default config.json file at first boot
#if [ ! -f /opt/mattermost/config/config.json.orig ]; then
#  cp -a /opt/mattermost/config/config.json /opt/mattermost/config/config.json.orig
#fi

#if [ "$DRIVER_NAME" = "mysql" ]; then
#    sed -e 's#"DriverName": "mysql"#"DriverName": "'"$DRIVER_NAME"'"#' \
#        -e 's#"DataSource": "mmuser:mostest@tcp(mysql:3306)/mattermost_test?charset=utf8mb4,utf8"#"DataSource": "'"$MYSQL_USER:$MYSQL_PASSWORD@tcp($(printenv ${DATABASE_SERVICE_NAME^^}_SERVICE_HOST):$(printenv ${DATABASE_SERVICE_NAME^^}_SERVICE_PORT))/$MYSQL_DATABASE?charset=utf8mb4,utf8"'"#' \
#        /opt/mattermost/config/config.json.orig > /opt/mattermost/config/config.json
#    sed -e 's#"DriverName": "mysql"#"DriverName": "'"$DRIVER_NAME"'"#' \
#        -e 's#"DataSource": "mmuser:mostest@tcp(mysql:3306)/mattermost_test?charset=utf8mb4,utf8"#"DataSource": #"'"mmuser:mmuser_password@mmopenpaas.cnym3nmwgp5j.ap-southeast-1.rds.amazonaws.com:3306/mattermost?charset=utf8mb4,utf8"'"#' \
#        /opt/mattermost/config/config.json.orig > /opt/mattermost/config/config.json
#fi

#echo -ne "Configure database connection..."
#if [ ! -f $config ]
#then
#    cp /opt/mattermost/config/config.template.json $config
#    sed -Ei "s/DB_HOST/$DB_HOST/" $config
#    sed -Ei "s/DB_PORT/$DB_PORT_5432_TCP_PORT/" $config
#    sed -Ei "s/MM_USERNAME/$MM_USERNAME/" $config
#    sed -Ei "s/MM_PASSWORD/$MM_PASSWORD/" $config
#    sed -Ei "s/MM_DBNAME/$MM_DBNAME/" $config
#    echo OK
#else
#    echo SKIP
#fi
#cat $config

cat /opt/mattermost/config/config.json

cd /opt/mattermost/bin/
#exec /opt/mattermost/bin/platform
